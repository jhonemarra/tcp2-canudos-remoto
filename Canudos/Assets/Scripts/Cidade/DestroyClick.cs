﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyClick : MonoBehaviour {

    void OnMouseDown()
    {
        // this object was clicked - do something
        Destroy(this.gameObject);
    }
}
