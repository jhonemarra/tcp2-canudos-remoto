﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager _Instancia;

    //___RECURSOS__________________________________________
    [Header("Recursos")]
    public int _barro;
    public int _pedra;
    public int _madeira;
    public int _comida;
    public int _populacao;
    public int _limitePopulacao;
    public Text _barroText, _pedraText, _madeiraText, _populacaoText, _comidaText, vidaText, forcaText, destrezaText, resistenciaText, vitalidadeText, energiaText;
    public Image backgroundAttribute;

    //___SOMA_DOS_RECURSOS__________________________________________
    [Header("Soma dos recursos")]
    public int _barroSoma;
    public int _pedraSoma;
    public int _madeiraSoma;
    public int _comidaSoma;
    public int _padraoSoma;
    //___Objetos_3D____________________________________________
    [Header("Objetos 3D")]
    //barro, pedra, medeira, comida, armazem, posto de troca, igreja, casas
    public GameObject _CasaNv1;
    public GameObject _CasaNv2, _CasaNv3;
    public GameObject _MercadoNv1, _MercadoNv2, _MercadoNv3;
    public GameObject _MadeiraNv1, _MadeiraNv2, _MadeiraNv3;
    public GameObject _BarroNv1, _BarroNv2, _BarroNv3;
    public GameObject _PedraNv1, _PedraNv2, _PedraNv3;
    public GameObject _ArmazemNv1, _ArmazemNv2, _ArmazemNv3;
    public GameObject _FazendaNv1, _FazendaNv2, _FazendaNv3;
    public GameObject _IgrejaNv1, _IgrejaNv2, _IgrejaNv3;
    [Header("Slots")]
    public GameObject _slotClicado;
    [Header("Objetos e outras variáveis")]
    //___OUTRAS VARIAVEIS________________________________________
    public float _timer = 0;
    public GameObject _camera;
    public Text _mensagemText;
    public float _contadorMensagem;
    string _Limpar = "";
    public bool _habilitarRecursos = true;
    public bool _habilitarConstrucoes = true;
    public GameObject _construcoes;
    public GameObject _messageBackground;
    public int _limiteArmazem;
    public int _consumo = 2;

    public float _cronometro, _timeCidade = 210;
    public int _roundAtual = 0;
    public bool _naCidade;
    public Text _roundText, _cronometroText;

    public int totalAllies = 2;
    public int totalEnemies = 2;


    void Awake()
    {
        if (_Instancia == null)
        {
            _Instancia = this;
        }
        else if (_Instancia != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start()
    {
        _barroSoma = _padraoSoma;
        _pedraSoma = _padraoSoma;
        _madeiraSoma = _padraoSoma;
        _messageBackground.SetActive(false);

        backgroundAttribute.enabled = false;
        vidaText.enabled = false;
        forcaText.enabled = false;
        destrezaText.enabled = false;
        resistenciaText.enabled = false;
        vitalidadeText.enabled = false;
        energiaText.enabled = false;

        _barro = 100;
        _madeira = 80;
        _pedra = 100;
        _comida = 50;
        _populacao = 2;
        _limitePopulacao = 2;
        _limiteArmazem = 100;

        ContagemRounds();
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;

        if (_naCidade == true)
        {
            _cronometro -= Time.deltaTime;
            _barroText.enabled = true;
            _pedraText.enabled = true;
            _madeiraText.enabled = true;
            _populacaoText.enabled = true;
            _comidaText.enabled = true;
        }
        else
        {
            _barroText.enabled = false;
            _pedraText.enabled = false;
            _madeiraText.enabled = false;
            _populacaoText.enabled = false;
            _comidaText.enabled = false;

            if (_barroText.GetComponentInChildren<Image>().enabled == true)
            {
                _barroText.GetComponentInChildren<Image>().enabled = false;
            }
            if (_pedraText.GetComponentInChildren<Image>().enabled == true)
            {
                _pedraText.GetComponentInChildren<Image>().enabled = false;
            }
            if (_madeiraText.GetComponentInChildren<Image>().enabled == true)
            {
                _madeiraText.GetComponentInChildren<Image>().enabled = false;
            }
            if (_populacaoText.GetComponentInChildren<Image>().enabled == true)
            {
                _populacaoText.GetComponentInChildren<Image>().enabled = false;
            }

        }

        if (_cronometro <= 0)
        {
            CarregarCena("Batalha");
            _cronometro = 0.1f;
            _naCidade = false;
        }

        _cronometroText.text = ((int)_cronometro).ToString();
        _roundText.text = "Round: " + _roundAtual;

        if (_timer > 10.2f && _habilitarRecursos == true)
        {
            _barro += _barroSoma;
            _pedra += _pedraSoma;
            _madeira += _madeiraSoma;
            _comida += _comidaSoma - (_consumo * _populacao);
            _timer = 0;
        }

        if (_populacao >= _limitePopulacao)
        {
            _populacao = _limitePopulacao;
        }

        LimitadorRecursos(_limiteArmazem);

        _barroText.text = "Barro: " + _barro + "/ " + _limiteArmazem;
        _pedraText.text = "Pedra: " + _pedra + "/ " + _limiteArmazem;
        _madeiraText.text = "Madeira: " + _madeira + "/ " + _limiteArmazem;
        _populacaoText.text = "População: " + _populacao + "/ " + _limitePopulacao;
        _comidaText.text = "Comida: " + _comida + "/ " + _limiteArmazem;

        _contadorMensagem += Time.deltaTime;
        //_HACK PARA TESTAR_____________________________________________________________________
        if (Input.GetKeyDown(KeyCode.X))
        {
            _barro += 100;
            _madeira += 100;
            _pedra += 100;
            _comida += 100;
        }

        if (_contadorMensagem > 2)
        {
            EscreverMensagem(_Limpar);
            _messageBackground.SetActive(false);
        }

        if (_habilitarConstrucoes == true && SceneManager.GetActiveScene().name == "Cidade")
        {
            _construcoes.SetActive(true);
        }
        else
        {
            _construcoes.SetActive(false);
        }
    }

    public void EscreverMensagem(string _texto)
    {
        _contadorMensagem = 0;
        _messageBackground.SetActive(true);
        _mensagemText.text = _texto;
    }

    public void CarregarCena(string _nomeDaCena)
    {
        _mensagemText.text = _Limpar;
        SceneManager.LoadScene(_nomeDaCena);
        _habilitarRecursos = false;
        _habilitarConstrucoes = false;
        FindObjectOfType<OnClickSlot>().FecharJanela();
        GameObject.Find("ancoraCamera").transform.position = new Vector3(-0.854162f, 7f, -4.015665f);
        GameObject.Find("ancoraCamera").transform.rotation = new Quaternion(0,0,0,0);

    }

    public void ContagemRounds()
    {
        if (_roundAtual == 0)
        {
            _timeCidade = 210;
            _cronometro = _timeCidade;
        }
        else if (_roundAtual == 1)
        {
            _timeCidade += 40;
            _cronometro = _timeCidade;
        }
        else if (_roundAtual == 2)
        {
            _timeCidade += 40;
            _cronometro = _timeCidade;
        }
        else if (_roundAtual >= 3)
        {
            _timeCidade += 30;
            _cronometro = _timeCidade;
        }
        _naCidade = true;
        _roundAtual += 1;
    }

    public void ZerarCronometro()
    {
        _cronometro = 0.1f;
    }

    void LimitadorRecursos(int _limite)
    {
        if (_barro >= _limite)
        {
            _barro = _limite;
        }
        if (_pedra >= _limite)
        {
            _pedra = _limite;
        }
        if (_madeira >= _limite)
        {
            _madeira = _limite;
        }
    }
}
