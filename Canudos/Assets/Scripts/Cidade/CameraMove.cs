﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{

    Vector3 _posicaoInicial;
    public GameObject center;
    float disCenter;

    private void Start()
    {
        _posicaoInicial = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        disCenter = Vector3.Distance(this.transform.position, center.transform.position);

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(new Vector3(-1, 0, 0));
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(new Vector3(1, 0, 0));
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(new Vector3(0, 0, 1));
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(new Vector3(0, 0, -1));
        }

        if (Input.mousePosition.x < Screen.width - Screen.width/20 * 1)
        {
            transform.RotateAround(Vector3.up, -1 * Time.deltaTime);
        }

        if (Input.mousePosition.x > Screen.width - Screen.width / 10 * 9)
        {
            transform.RotateAround(Vector3.up, +1 * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            transform.position = _posicaoInicial;
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0f && transform.position.y <= 30 && transform.position.y >= 7) // forward
        {
            transform.position -= new Vector3(0, 500 * Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime, 0);
        }
        if (transform.position.y > 30)
        {
            transform.position = new Vector3(transform.position.x,30, transform.position.z);
        }
        if (transform.position.y < 7)
        {
            transform.position = new Vector3(transform.position.x, 7, transform.position.z);
        }

        if (transform.position.z < -100)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -100);
        }
        if (transform.position.z > 100)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 100);
        }

        if (transform.position.x < -135)
        {
            transform.position = new Vector3(-135, transform.position.y, transform.position.z);

        }
        if (transform.position.x > 135)
        {
            transform.position = new Vector3(135, transform.position.y, transform.position.z);

        }

        if (Input.GetKey(KeyCode.PageDown) && (transform.position.y > 7))
        {
            transform.position += Vector3.down;
        }
    }
}
