﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DontDestroy : MonoBehaviour {
    public static DontDestroy _Instancia;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
