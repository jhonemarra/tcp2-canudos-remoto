﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayIntro : MonoBehaviour
{
    float _contador;

    private void Update()
    {
        _contador += Time.deltaTime;
        if (_contador > 8)
        {
            SceneManager.LoadScene("Menu Iniciar");
        }
    }
}
