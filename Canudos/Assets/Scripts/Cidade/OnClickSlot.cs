﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnClickSlot : MonoBehaviour {

    public GameObject _Janela1;
    //public string _construcao;
    public AudioClip _efeitoClick, _efeitoMensagem;
    public GameObject _objetoAtual;
    GameObject[] _slots;

    void Start ()
    {
        //_construcao = this.gameObject.tag.ToString();
        _Janela1.SetActive(false);
        _slots = GameObject.FindGameObjectsWithTag("slot");
    }
	
	void Update ()
    {

    }

    private void OnMouseDown()
    {
        GameManager._Instancia._slotClicado = this.gameObject;
        if (this.gameObject.name == "0" || this.gameObject.name == "Armazem" || this.gameObject.name == "1" || this.gameObject.name == "2")
        {
            _Janela1.SetActive(true);
            for(int i = 0; i < _slots.Length; i++)
            {
                _slots[i].GetComponent<Collider>().enabled = false;
            }
        }
        else
        {
            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
            GameManager._Instancia.EscreverMensagem("Este edifício está no nível máximo.");
        }
    }

    public void FecharJanela()
    {
        _Janela1.SetActive(false);
        for (int i = 0; i < _slots.Length; i++)
        {
            _slots[i].GetComponent<Collider>().enabled = true;
        }
    }
}
