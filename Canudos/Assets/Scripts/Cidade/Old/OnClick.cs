﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;
//using UnityEngine.EventSystems;

//public class OnClick : MonoBehaviour {

//    public string _construcao;
//    public AudioClip _efeitoClick, _efeitoMensagem;
//    public Button _esteBotao;
//    float _cooldown = 0;

//    private void Start()
//    {
//        _construcao = this.gameObject.tag.ToString();
//        _esteBotao = GetComponent<Button>();
//        _esteBotao.onClick.AddListener(() => BotaoAcionado());
//    }

//    private void Update()
//    {
//        _cooldown += Time.deltaTime;
//    }

//    private void OnMouseOver()
//    {
//        Debug.Log("Passou o mouse");
//        if(_construcao == "populacao")
//        {
//            if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 0.ToString())
//            {
//                GameManager._Instancia.EscreverMensagem("Madeira: 40 | Barro: 60");
//            }else
//            {
//                if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 1.ToString())
//                {
//                    GameManager._Instancia.EscreverMensagem("Madeira: 92 | Barro: 138 | Pedra: 100");
//                }else
//                {
//                    if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 2.ToString())
//                    {
//                        GameManager._Instancia.EscreverMensagem("Madeira: 432 | Barro: 60 | Pedra: 322");
//                    }
//                    else
//                    {
//                        if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 3.ToString())
//                        {
//                            GameManager._Instancia.EscreverMensagem("Este edifício está no nível máximo");
//                        }
//                    }
//                }
//            }
//        }
//    }

//    void BotaoAcionado()
//    {
//        if (int.Parse(GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name) < 3)
//        {
//            if (_construcao == "madeira")
//            {
//                if (GameManager._Instancia._madeira >= 20)
//                {
//                    if(GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 1.ToString())
//                    {
//                        GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 2.ToString();
//                    }
//                    else
//                    {
//                        if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 2.ToString())
//                        {
//                            GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 3.ToString();
//                        }
//                    }
//                    GameManager._Instancia._madeira -= 20;
//                    GameManager._Instancia._madeiraSoma += 6;
//                    SoundManager._Instancia.PlayEffect(_efeitoClick);
//                }
//                else
//                {
//                    GameManager._Instancia.EscreverMensagem("Você não possuí recursos suficientes para contruir este edifício.");
//                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
//                }
//            }
//            if (_construcao == "barro")
//            {
//                if (GameManager._Instancia._barro >= 20)
//                {
//                    if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 1.ToString())
//                    {
//                        GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 2.ToString();
//                    }
//                    else
//                    {
//                        if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 2.ToString())
//                        {
//                            GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 3.ToString();
//                        }
//                    }
//                    GameManager._Instancia._barro -= 20;
//                    GameManager._Instancia._barroSoma += 6;
//                    SoundManager._Instancia.PlayEffect(_efeitoClick);
//                }
//                else
//                {
//                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
//                    GameManager._Instancia.EscreverMensagem("Você não possuí recursos suficientes para contruir este edifício.");
//                }
//            }
//            if (_construcao == "pedra")
//            {
//                if (GameManager._Instancia._pedra >= 20)
//                {
//                    if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 1.ToString())
//                    {
//                        GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 2.ToString();
//                    }
//                    else
//                    {
//                        if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 2.ToString())
//                        {
//                            GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 3.ToString();
//                        }
//                    }
//                    GameManager._Instancia._pedra -= 20;
//                    GameManager._Instancia._pedraSoma += 6;
//                    SoundManager._Instancia.PlayEffect(_efeitoClick);
//                }
//                else
//                {
//                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
//                    GameManager._Instancia.EscreverMensagem("Você não possuí recursos suficientes para contruir este edifício.");
//                }
//            }
//            //if (_construcao == "populacao")
//            //{
//            //    if (GameManager._Instancia._madeira >= 50 && GameManager._Instancia._barro >= 50 
//            //            && GameManager._Instancia._pedra >= 50)
//            //    {
//            //        if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 0.ToString())
//            //        {
//            //            Vector3 _diferencaAltura = new Vector3(-1, 2,-1);
//            //            Instantiate(GameManager._Instancia._CasaNv1, GameManager._Instancia._slots[GameManager._Instancia._slotClicado].transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
//            //            GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 1.ToString();
//            //            GameObject _objetoInstanciado = GameObject.Find("CasaLv1(Clone)");
//            //            _objetoInstanciado.name = "Casa" + GameManager._Instancia._slotClicado.ToString();
//            //            _objetoInstanciado.transform.SetParent(GameObject.Find(GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name).transform);
//            //        }
//            //        else
//            //        {
//            //            if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 1.ToString())
//            //            {
//            //                GameObject _objetoAnterior = GameObject.Find("Casa" + GameManager._Instancia._slotClicado.ToString());
//            //                Destroy(_objetoAnterior);
//            //                Vector3 _diferencaAltura = new Vector3(1, 2,-1);
//            //                Instantiate(GameManager._Instancia._CasaNv2, GameManager._Instancia._slots[GameManager._Instancia._slotClicado].transform.position + _diferencaAltura, Quaternion.Euler(-90, 135, 0));
//            //                GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 2.ToString();
//            //                GameObject _objetoInstanciado = GameObject.Find("CasaLv2(Clone)");
//            //                _objetoInstanciado.name = "Casa" + GameManager._Instancia._slotClicado.ToString();
//            //                _objetoInstanciado.transform.SetParent(GameObject.Find(GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name).transform);
//            //            }
//            //            else
//            //            {
//            //                if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 2.ToString())
//            //                {
//            //                    GameObject _objetoAnterior = GameObject.Find("Casa" + GameManager._Instancia._slotClicado.ToString());
//            //                    Destroy(_objetoAnterior);
//            //                    Vector3 _diferencaAltura = new Vector3(0, 7, 1);
//            //                    Instantiate(GameManager._Instancia._CasaNv3, GameManager._Instancia._slots[GameManager._Instancia._slotClicado].transform.position + _diferencaAltura, Quaternion.Euler(-90, -45, 0));
//            //                    GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 3.ToString();
//            //                    GameObject _objetoInstanciado = GameObject.Find("CasaLv3(Clone)");
//            //                    _objetoInstanciado.name = "Casa" + GameManager._Instancia._slotClicado.ToString();
//            //                    _objetoInstanciado.transform.SetParent(GameObject.Find(GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name).transform);
//            //                }
//            //            }
//            //        }
//            //        //if (_buildNivel == 0)
//            //        //{
//            //        //    Instantiate(GameManager._Instancia._CasaNv1, GameManager._Instancia._slots[GameManager._Instancia._slotClicado].transform.position, Quaternion.identity);
//            //        //}
//            //        //else
//            //        //{
//            //        //    if (_buildNivel == 1)
//            //        //    {
//            //        //        Instantiate(GameManager._Instancia._CasaNv2, GameManager._Instancia._slots[GameManager._Instancia._slotClicado].transform.position, Quaternion.identity);
//            //        //    }
//            //        //    else
//            //        //    {
//            //        //        if (_buildNivel == 2)
//            //        //        {
//            //        //            Instantiate(GameManager._Instancia._CasaNv3, GameManager._Instancia._slots[GameManager._Instancia._slotClicado].transform.position, Quaternion.identity);
//            //        //        }
//            //        //    }
//            //        //}
//            //        //_buildNivel += 1;
//            //        GameManager._Instancia._pedra -= 50;
//            //        GameManager._Instancia._madeira -= 50;
//            //        GameManager._Instancia._barro -= 50;
//            //        GameManager._Instancia._limitePopulacao += 8;
//            //        SoundManager._Instancia.PlayEffect(_efeitoClick);
//            //    }
//            //    else
//            //    {
//            //        SoundManager._Instancia.PlayEffect(_efeitoMensagem);
//            //        GameManager._Instancia.EscreverMensagem("Você não possuí recursos suficientes para contruir este edifício.");
//            //    }
//            //}

//            if (_construcao == "populacao")
//            {
//                if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 0.ToString())
//                {
//                    if (GameManager._Instancia._madeira >= 40 && GameManager._Instancia._barro >= 60)
//                    {
//                        Vector3 _diferencaAltura = new Vector3(-1, 2, -1);
//                        Instantiate(GameManager._Instancia._CasaNv1, GameManager._Instancia._slots[GameManager._Instancia._slotClicado].transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
//                        GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 1.ToString();
//                        GameObject _objetoInstanciado = GameObject.Find("CasaLv1(Clone)");
//                        _objetoInstanciado.name = "Casa" + GameManager._Instancia._slotClicado.ToString();
//                        _objetoInstanciado.transform.SetParent(GameObject.Find(GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name).transform);
//                        GameManager._Instancia._madeira -= 40;
//                        GameManager._Instancia._barro -= 60;
//                        GameManager._Instancia._limitePopulacao += 1;
//                        SoundManager._Instancia.PlayEffect(_efeitoClick);
//                        //Colwndown 25 seundos
//                    }
//                    else
//                    {
//                        SoundManager._Instancia.PlayEffect(_efeitoMensagem);
//                        GameManager._Instancia.EscreverMensagem("Você precisa de 40 unidades de madeira e 60 unidades barro para contruir este edifício.");
//                    }
//                }
//                else
//                {
//                    if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 1.ToString())
//                    {
//                        if (GameManager._Instancia._madeira >= 92 && GameManager._Instancia._barro >= 138 && GameManager._Instancia._pedra >= 100)
//                        {
//                            GameObject _objetoAnterior = GameObject.Find("Casa" + GameManager._Instancia._slotClicado.ToString());
//                            Destroy(_objetoAnterior);
//                            Vector3 _diferencaAltura = new Vector3(1, 2, -1);
//                            Instantiate(GameManager._Instancia._CasaNv2, GameManager._Instancia._slots[GameManager._Instancia._slotClicado].transform.position + _diferencaAltura, Quaternion.Euler(-90, 135, 0));
//                            GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 2.ToString();
//                            GameObject _objetoInstanciado = GameObject.Find("CasaLv2(Clone)");
//                            _objetoInstanciado.name = "Casa" + GameManager._Instancia._slotClicado.ToString();
//                            _objetoInstanciado.transform.SetParent(GameObject.Find(GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name).transform);
//                            GameManager._Instancia._madeira -= 92;
//                            GameManager._Instancia._barro -= 138;
//                            GameManager._Instancia._pedra -= 100;
//                            GameManager._Instancia._limitePopulacao += 2;
//                            SoundManager._Instancia.PlayEffect(_efeitoClick);
//                            //Colwndown 30 seundos
//                        }
//                        else
//                        {
//                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
//                            GameManager._Instancia.EscreverMensagem("Você precisa de 92 unidades de madeira, 138 unidades barro e 100 unidades de pedra para contruir este edifício.");
//                        }
//                    } else
//                    {
//                        if (GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name == 2.ToString())
//                        {
//                            if (GameManager._Instancia._madeira >= 423 && GameManager._Instancia._barro >= 60 && GameManager._Instancia._pedra >= 322)
//                            {
//                                GameObject _objetoAnterior = GameObject.Find("Casa" + GameManager._Instancia._slotClicado.ToString());
//                                Destroy(_objetoAnterior);
//                                Vector3 _diferencaAltura = new Vector3(0, 7, 1);
//                                Instantiate(GameManager._Instancia._CasaNv3, GameManager._Instancia._slots[GameManager._Instancia._slotClicado].transform.position + _diferencaAltura, Quaternion.Euler(-90, -45, 0));
//                                GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name = 3.ToString();
//                                GameObject _objetoInstanciado = GameObject.Find("CasaLv3(Clone)");
//                                _objetoInstanciado.name = "Casa" + GameManager._Instancia._slotClicado.ToString();
//                                _objetoInstanciado.transform.SetParent(GameObject.Find(GameManager._Instancia._slots[GameManager._Instancia._slotClicado].name).transform);
//                                GameManager._Instancia._madeira -= 423;
//                                GameManager._Instancia._barro -= 60;
//                                GameManager._Instancia._pedra -= 322;
//                                GameManager._Instancia._limitePopulacao += 4;
//                                SoundManager._Instancia.PlayEffect(_efeitoClick);
//                                //Colwndown 35 seundos
//                            }else
//                            {
//                                SoundManager._Instancia.PlayEffect(_efeitoMensagem);
//                                GameManager._Instancia.EscreverMensagem("Você precisa de 423 unidades de madeira, 60 unidades barro e 322 unidades de pedra para contruir este edifício.");
//                            }
//                        }
//                    }
//                }
//            }

//        }
//        else
//        {
//            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
//            GameManager._Instancia.EscreverMensagem("Este edifício está no nível máximo.");
//        }
//    }
//}
