﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnClickJanela : MonoBehaviour
{
    public string _construcao;
    public AudioClip _efeitoClick, _efeitoMensagem;
    public Button _esteBotao;
    GameObject _objetoAtual, _slotClicado;
    bool _jaExisteArmazem = false;
    Igreja igreja;

    void Start()
    {
        _construcao = this.gameObject.tag.ToString();
        _esteBotao = GetComponent<Button>();
        _esteBotao.onClick.AddListener(() => BotaoAcionado());
        igreja = FindObjectOfType<Igreja>();

    }

    void Update()
    {

    }

    void BotaoAcionado()
    {
        _slotClicado = GameManager._Instancia._slotClicado;
        if (_construcao == "populacao")
        {
            if (_slotClicado.name == "0")
            {
                if (GameManager._Instancia._madeira >= 40 && GameManager._Instancia._barro >= 60)
                {
                    Vector3 _diferencaAltura = new Vector3(-6,-0.8f,-4);
                    Instantiate(GameManager._Instancia._CasaNv1, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                    _slotClicado.name = 1.ToString();
                    _objetoAtual = GameObject.Find("CasasLv1(Clone)");
                    _objetoAtual.name = "Casa" + _slotClicado.gameObject.name;
                    _objetoAtual.transform.SetParent(_slotClicado.transform);
                    _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                    GameManager._Instancia._madeira -= 40;
                    GameManager._Instancia._barro -= 60;
                    GameManager._Instancia._limitePopulacao += 1;
                    SoundManager._Instancia.PlayEffect(_efeitoClick);
                    GameManager._Instancia.EscreverMensagem("uma Casa nv.1 foi construída!");
                    //Colwndown 25 seundos
                }
                else
                {
                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                    GameManager._Instancia.EscreverMensagem("Você precisa de 40 unidades de madeira e 60 unidades barro para contruir este edifício.");
                }
            }
            else
            {
                if (_slotClicado.name == "1")
                {
                    if (GameManager._Instancia._madeira >= 92 && GameManager._Instancia._barro >= 138 && GameManager._Instancia._pedra >= 100)
                    {
                        Destroy(_slotClicado.GetComponent<OnClickSlot>()._objetoAtual);
                        Vector3 _diferencaAltura = new Vector3(1, 2, -1);
                        Instantiate(GameManager._Instancia._CasaNv2, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 135, 0));
                        _slotClicado.name = 2.ToString();
                        _objetoAtual = GameObject.Find("CasaLv2(Clone)");
                        _objetoAtual.name = "Casa" + _slotClicado.name;
                        _objetoAtual.transform.SetParent(_slotClicado.transform);
                        _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                        GameManager._Instancia._madeira -= 92;
                        GameManager._Instancia._barro -= 138;
                        GameManager._Instancia._pedra -= 100;
                        GameManager._Instancia._limitePopulacao += 2;
                        SoundManager._Instancia.PlayEffect(_efeitoClick);
                        GameManager._Instancia.EscreverMensagem("uma Casa nv.2 foi construída!");
                        //Colwndown 30 seundos
                    }
                    else
                    {
                        SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                        GameManager._Instancia.EscreverMensagem("Você precisa de 92 unidades de madeira, 138 unidades barro e 100 unidades de pedra para contruir este edifício.");
                    }
                }
                else
                {
                    if (_slotClicado.name == "2")
                    {
                        if (GameManager._Instancia._madeira >= 423 && GameManager._Instancia._barro >= 60 && GameManager._Instancia._pedra >= 322)
                        {
                            Destroy(_slotClicado.GetComponent<OnClickSlot>()._objetoAtual);
                            Vector3 _diferencaAltura = new Vector3(0, 7, 1);
                            Instantiate(GameManager._Instancia._CasaNv3, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, -45, 0));
                            _slotClicado.name = 3.ToString();
                            _objetoAtual = GameObject.Find("CasaLv3(Clone)");
                            _objetoAtual.name = "Casa" + _slotClicado.name;
                            _objetoAtual.transform.SetParent(_slotClicado.transform);
                            _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                            GameManager._Instancia._madeira -= 423;
                            GameManager._Instancia._barro -= 60;
                            GameManager._Instancia._pedra -= 322;
                            GameManager._Instancia._limitePopulacao += 4;
                            SoundManager._Instancia.PlayEffect(_efeitoClick);
                            GameManager._Instancia.EscreverMensagem("uma Casa nv.3 foi construída!");
                            //Colwndown 35 seundos
                        }
                        else
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Você precisa de 423 unidades de madeira, 60 unidades barro e 322 unidades de pedra para contruir este edifício.");
                        }
                    }
                }
            }
        }

        if (_construcao == "armazem")
        {
            if (_jaExisteArmazem == false)
            {
                if(_slotClicado.name == "0")
                {
                    if (GameManager._Instancia._madeira >= 35 && GameManager._Instancia._barro >= 40)
                    {
                        Vector3 _diferencaAltura = new Vector3(0, 2, 0);
                        Instantiate(GameManager._Instancia._ArmazemNv1, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                        _objetoAtual = GameObject.Find("Armazem(Clone)");
                        _objetoAtual.transform.SetParent(_slotClicado.transform);
                        _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                        _slotClicado.name = "Armazem";
                        _slotClicado.GetComponent<OnClickSlot>()._objetoAtual.name = 1.ToString();
                        GameManager._Instancia._madeira -= 35;
                        GameManager._Instancia._barro -= 40;
                        GameManager._Instancia._limiteArmazem = 200;
                        SoundManager._Instancia.PlayEffect(_efeitoClick);
                        GameManager._Instancia.EscreverMensagem("O Armazém foi construído!");
                        _jaExisteArmazem = true;
                    }
                }else
                {
                    GameManager._Instancia.EscreverMensagem("Já existe uma construção nesse lugar.");
                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                }
            }
            else
            {
                if(_jaExisteArmazem == true && _slotClicado.name != "Armazem")
                {
                    GameManager._Instancia.EscreverMensagem("Um armazém já foi construído.");
                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                }
                else
                {
                    if (_jaExisteArmazem == true && _slotClicado.name == "Armazem" && _slotClicado.GetComponent<OnClickSlot>()._objetoAtual.name == 1.ToString())
                    {
                        if (GameManager._Instancia._madeira >= 91 && GameManager._Instancia._barro >= 104 && GameManager._Instancia._pedra >= 80)
                        {
                            Destroy(_slotClicado.GetComponent<OnClickSlot>()._objetoAtual);
                            Vector3 _diferencaAltura = new Vector3(0, 3, 0);
                            Instantiate(GameManager._Instancia._ArmazemNv2, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                            _objetoAtual = GameObject.Find("oldArmazém(Clone)");
                            _objetoAtual.transform.SetParent(_slotClicado.transform);
                            _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                            _slotClicado.GetComponent<OnClickSlot>()._objetoAtual.name = 2.ToString();
                            GameManager._Instancia._madeira -= 91;
                            GameManager._Instancia._barro -= 104;
                            GameManager._Instancia._pedra -= 80;
                            GameManager._Instancia._limiteArmazem = 400;
                            SoundManager._Instancia.PlayEffect(_efeitoClick);
                            GameManager._Instancia.EscreverMensagem("O Armazém foi evoluído para o nível 2!");
                        }else
                        {
                            GameManager._Instancia.EscreverMensagem("Você precisa de 104 unidade de barro, 91 unidades de madeira e 80 unidades de pedra para construir este edifício.");
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                        }
                    }
                    else
                    {
                        if(_jaExisteArmazem == true && _slotClicado.name == "Armazem" && _slotClicado.GetComponent<OnClickSlot>()._objetoAtual.name == 2.ToString())
                        {
                            if(GameManager._Instancia._madeira >= 236 && GameManager._Instancia._barro >= 135 && GameManager._Instancia._pedra >= 208)
                            {
                                Destroy(_slotClicado.GetComponent<OnClickSlot>()._objetoAtual);
                                Vector3 _diferencaAltura = new Vector3(0, 4, 0);
                                Instantiate(GameManager._Instancia._ArmazemNv3, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                                _objetoAtual = GameObject.Find("oldArmazém(Clone)");
                                _objetoAtual.transform.SetParent(_slotClicado.transform);
                                _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                                _slotClicado.GetComponent<OnClickSlot>()._objetoAtual.name = 3.ToString();
                                GameManager._Instancia._madeira -= 236;
                                GameManager._Instancia._barro -= 135;
                                GameManager._Instancia._pedra -= 208;
                                GameManager._Instancia._limiteArmazem = 600;
                                SoundManager._Instancia.PlayEffect(_efeitoClick);
                                GameManager._Instancia.EscreverMensagem("O Armazém foi evoluído para o nível 3!");
                            }
                            else
                            {
                                GameManager._Instancia.EscreverMensagem("Você precisa de 135 unidades de barro, 236 unidades de madeira e 208 unidades de pedra para construir esse edifício.");
                                SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            }
                        }
                    }
                } 
            }
        }

        if (_construcao == "barro")
        {
            if (_slotClicado.name == "0")
            {
                if (GameManager._Instancia._madeira >= 20 && GameManager._Instancia._barro >= 45)
                {
                    Vector3 _diferencaAltura = new Vector3(0, 2, 0);
                    Instantiate(GameManager._Instancia._BarroNv1, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                    _slotClicado.name = 1.ToString();
                    _objetoAtual = GameObject.Find("Barro(Clone)");
                    _objetoAtual.name = "Barro" + _slotClicado.gameObject.name;
                    _objetoAtual.transform.SetParent(_slotClicado.transform);
                    _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                    igreja.barroMax += 1;
                    igreja.CalculateLimits();
                    GameManager._Instancia._madeira -= 20;
                    GameManager._Instancia._barro -= 45;
                    GameManager._Instancia._barroSoma += 5;
                    SoundManager._Instancia.PlayEffect(_efeitoClick);
                    GameManager._Instancia.EscreverMensagem("Um edifício produtor de barro foi construído!");
                }
                else
                {
                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                    GameManager._Instancia.EscreverMensagem("Você precisa de 20 unidades de madeira e 45 unidades barro para contruir este edifício.");
                }
            }
            else
            {
                if (_slotClicado.name == "1")
                {
                    if (GameManager._Instancia._madeira >= 56 && GameManager._Instancia._barro >= 126 && GameManager._Instancia._pedra >= 60)
                    {
                        Vector3 _diferencaAltura = new Vector3(0, 3, 0);
                        Instantiate(GameManager._Instancia._BarroNv2, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                        _slotClicado.name = 2.ToString();
                        _objetoAtual = GameObject.Find("Barro(Clone)");
                        _objetoAtual.name = "Barro" + _slotClicado.gameObject.name;
                        _objetoAtual.transform.SetParent(_slotClicado.transform);
                        _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                        igreja.barroMax += 2;
                        igreja.CalculateLimits();
                        GameManager._Instancia._madeira -= 56;
                        GameManager._Instancia._barro -= 126;
                        GameManager._Instancia._pedra -= 60;
                        GameManager._Instancia._barroSoma += 12;
                        SoundManager._Instancia.PlayEffect(_efeitoClick);
                        GameManager._Instancia.EscreverMensagem("Um edifício produtor de barro foi evoluído!");
                    }
                    else
                    {
                        SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                        GameManager._Instancia.EscreverMensagem("Você precisa de 56 unidades de madeira, 60 unidades de pedra e 126 unidades barro para contruir este edifício.");
                    }
                }
                else
                {
                    if(_slotClicado.name == "2")
                    {
                        if (GameManager._Instancia._madeira >= 156 && GameManager._Instancia._barro >= 176 && GameManager._Instancia._pedra >= 168)
                        {
                            Vector3 _diferencaAltura = new Vector3(0, 4, 0);
                            Instantiate(GameManager._Instancia._BarroNv3, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                            _slotClicado.name = 3.ToString();
                            _objetoAtual = GameObject.Find("Barro(Clone)");
                            _objetoAtual.name = "Barro" + _slotClicado.gameObject.name;
                            _objetoAtual.transform.SetParent(_slotClicado.transform);
                            _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                            igreja.barroMax += 3;
                            igreja.CalculateLimits();
                            GameManager._Instancia._madeira -= 156;
                            GameManager._Instancia._barro -= 176;
                            GameManager._Instancia._pedra -= 168;
                            GameManager._Instancia._barroSoma += 28;
                            SoundManager._Instancia.PlayEffect(_efeitoClick);
                            GameManager._Instancia.EscreverMensagem("Um edifício produtor de barro foi evoluído!");
                        }
                        else
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Você precisa de 156 unidades de madeira, 168 unidades de pedra e 176 unidades barro para contruir este edifício.");
                        }
                    }else
                    {
                        if(_slotClicado.name == "3")
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Este edifício já está no nível máximo.");
                        }
                    }
                }
            }
        }

        if (_construcao == "pedra")
        {
            if (_slotClicado.name == "0")
            {
                if (GameManager._Instancia._madeira >= 45 && GameManager._Instancia._barro >= 30 && GameManager._Instancia._pedra >= 27)
                {
                    Vector3 _diferencaAltura = new Vector3(0, 2, 0);
                    Instantiate(GameManager._Instancia._PedraNv1, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                    _slotClicado.name = 1.ToString();
                    _objetoAtual = GameObject.Find("Pedra(Clone)");
                    _objetoAtual.name = "Pedra" + _slotClicado.gameObject.name;
                    _objetoAtual.transform.SetParent(_slotClicado.transform);
                    _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                    igreja.pedraMax += 1;
                    igreja.CalculateLimits();
                    GameManager._Instancia._madeira -= 45;
                    GameManager._Instancia._barro -= 30;
                    GameManager._Instancia._pedra -= 27;
                    GameManager._Instancia._pedraSoma += 4;
                    SoundManager._Instancia.PlayEffect(_efeitoClick);
                    GameManager._Instancia.EscreverMensagem("Um edifício produtor de pedra foi construído!");
                }
                else
                {
                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                    GameManager._Instancia.EscreverMensagem("Você precisa de 45 unidades de madeira, 27 unidades de pedra e 30 unidades barro para contruir este edifício.");
                }
            }
            else
            {
                if (_slotClicado.name == "1")
                {
                    if (GameManager._Instancia._madeira >= 126 && GameManager._Instancia._barro >= 84 && GameManager._Instancia._pedra >= 105)
                    {
                        Vector3 _diferencaAltura = new Vector3(0, 3, 0);
                        Instantiate(GameManager._Instancia._PedraNv2, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                        _slotClicado.name = 2.ToString();
                        _objetoAtual = GameObject.Find("Pedra(Clone)");
                        _objetoAtual.name = "Pedra" + _slotClicado.gameObject.name;
                        _objetoAtual.transform.SetParent(_slotClicado.transform);
                        _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                        igreja.pedraMax += 2;
                        igreja.CalculateLimits();
                        GameManager._Instancia._madeira -= 126;
                        GameManager._Instancia._barro -= 84;
                        GameManager._Instancia._pedra -= 105;
                        GameManager._Instancia._pedraSoma += 8;
                        SoundManager._Instancia.PlayEffect(_efeitoClick);
                        GameManager._Instancia.EscreverMensagem("Um edifício produtor de pedra foi evoluído!");
                    }
                    else
                    {
                        SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                        GameManager._Instancia.EscreverMensagem("Você precisa de 126 unidades de madeira, 105 unidades de pedra e 84 unidades barro para contruir este edifício.");
                    }
                }
                else
                {
                    if (_slotClicado.name == "2")
                    {
                        if (GameManager._Instancia._madeira >= 352 && GameManager._Instancia._barro >= 117 && GameManager._Instancia._pedra >= 296)
                        {
                            Vector3 _diferencaAltura = new Vector3(0, 4, 0);
                            Instantiate(GameManager._Instancia._PedraNv3, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                            _slotClicado.name = 3.ToString();
                            _objetoAtual = GameObject.Find("Pedra(Clone)");
                            _objetoAtual.name = "Pedra" + _slotClicado.gameObject.name;
                            _objetoAtual.transform.SetParent(_slotClicado.transform);
                            _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                            igreja.pedraMax += 3;
                            igreja.CalculateLimits();
                            GameManager._Instancia._madeira -= 352;
                            GameManager._Instancia._barro -= 117;
                            GameManager._Instancia._pedra -= 296;
                            GameManager._Instancia._pedraSoma += 18;
                            SoundManager._Instancia.PlayEffect(_efeitoClick);
                            GameManager._Instancia.EscreverMensagem("Um edifício produtor de pedra foi evoluído!");
                        }
                        else
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Você precisa de 352 unidades de madeira, 296 unidades de pedra e 117 unidades barro para contruir este edifício.");
                        }
                    }
                    else
                    {
                        if (_slotClicado.name == "3")
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Este edifício já está no nível máximo.");
                        }
                    }
                }
            }
        }

        if (_construcao == "madeira")
        {
            if (_slotClicado.name == "0")
            {
                if (GameManager._Instancia._madeira >= 25 && GameManager._Instancia._barro >= 55)
                {
                    Vector3 _diferencaAltura = new Vector3(0, 2, 0);
                    Instantiate(GameManager._Instancia._MadeiraNv1, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                    _slotClicado.name = 1.ToString();
                    _objetoAtual = GameObject.Find("Madeira(Clone)");
                    _objetoAtual.name = "Madeira" + _slotClicado.gameObject.name;
                    _objetoAtual.transform.SetParent(_slotClicado.transform);
                    _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                    igreja.madeiraMax += 1;
                    igreja.CalculateLimits();
                    GameManager._Instancia._madeira -= 25;
                    GameManager._Instancia._barro -= 55;
                    GameManager._Instancia._madeiraSoma += 4;
                    SoundManager._Instancia.PlayEffect(_efeitoClick);
                    GameManager._Instancia.EscreverMensagem("Um edifício produtor de madeira foi construído!");
                }
                else
                {
                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                    GameManager._Instancia.EscreverMensagem("Você precisa de 25 unidades de madeira e 55 unidades barro para contruir este edifício.");
                }
            }
            else
            {
                if (_slotClicado.name == "1")
                {
                    if (GameManager._Instancia._madeira >= 70 && GameManager._Instancia._barro >= 154 && GameManager._Instancia._pedra >= 60)
                    {
                        Vector3 _diferencaAltura = new Vector3(0, 3, 0);
                        Instantiate(GameManager._Instancia._MadeiraNv2, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                        _slotClicado.name = 2.ToString();
                        _objetoAtual = GameObject.Find("Madeira(Clone)");
                        _objetoAtual.name = "Madeira" + _slotClicado.gameObject.name;
                        _objetoAtual.transform.SetParent(_slotClicado.transform);
                        _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                        igreja.madeiraMax += 2;
                        igreja.CalculateLimits();
                        GameManager._Instancia._madeira -= 70;
                        GameManager._Instancia._barro -= 154;
                        GameManager._Instancia._pedra -= 60;
                        GameManager._Instancia._madeiraSoma += 8;
                        SoundManager._Instancia.PlayEffect(_efeitoClick);
                        GameManager._Instancia.EscreverMensagem("Um edifício produtor de madeira foi evoluído!");
                    }
                    else
                    {
                        SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                        GameManager._Instancia.EscreverMensagem("Você precisa de 70 unidades de madeira, 60 unidades de pedra e 154 unidades barro para contruir este edifício.");
                    }
                }
                else
                {
                    if (_slotClicado.name == "2")
                    {
                        if (GameManager._Instancia._madeira >= 196 && GameManager._Instancia._barro >= 215 && GameManager._Instancia._pedra >= 168)
                        {
                            Vector3 _diferencaAltura = new Vector3(0, 4, 0);
                            Instantiate(GameManager._Instancia._MadeiraNv3, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                            _slotClicado.name = 3.ToString();
                            _objetoAtual = GameObject.Find("Madeira(Clone)");
                            _objetoAtual.name = "Madeira" + _slotClicado.gameObject.name;
                            _objetoAtual.transform.SetParent(_slotClicado.transform);
                            _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                            igreja.madeiraMax += 3;
                            igreja.CalculateLimits();
                            GameManager._Instancia._madeira -= 196;
                            GameManager._Instancia._barro -= 215;
                            GameManager._Instancia._pedra -= 168;
                            GameManager._Instancia._madeiraSoma += 18;
                            SoundManager._Instancia.PlayEffect(_efeitoClick);
                            GameManager._Instancia.EscreverMensagem("Um edifício produtor de madeira foi evoluído!");
                        }
                        else
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Você precisa de 196 unidades de madeira, 168 unidades de pedra e 215 unidades barro para contruir este edifício.");
                        }
                    }
                    else
                    {
                        if (_slotClicado.name == "3")
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Este edifício já está no nível máximo.");
                        }
                    }
                }
            }
        }

        if (_construcao == "fazenda")
        {
            if (_slotClicado.name == "0")
            {
                if (GameManager._Instancia._madeira >= 20 && GameManager._Instancia._barro >= 30)
                {
                    Vector3 _diferencaAltura = new Vector3(0, 2, 0);
                    Instantiate(GameManager._Instancia._FazendaNv1, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                    _slotClicado.name = 1.ToString();
                    _objetoAtual = GameObject.Find("Fazenda(Clone)");
                    _objetoAtual.name = "Fazenda" + _slotClicado.gameObject.name;
                    _objetoAtual.transform.SetParent(_slotClicado.transform);
                    _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                    igreja.comidaMax += 1;
                    igreja.CalculateLimits();
                    GameManager._Instancia._madeira -= 20;
                    GameManager._Instancia._barro -= 30;
                    GameManager._Instancia._comidaSoma += 3;
                    SoundManager._Instancia.PlayEffect(_efeitoClick);
                    GameManager._Instancia.EscreverMensagem("Uma fazenda foi construída!");
                }
                else
                {
                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                    GameManager._Instancia.EscreverMensagem("Você precisa de 20 unidades de madeira e 30 unidades barro para contruir este edifício.");
                }
            }
            else
            {
                if (_slotClicado.name == "1")
                {
                    if (GameManager._Instancia._madeira >= 54 && GameManager._Instancia._barro >= 81 && GameManager._Instancia._pedra >= 60)
                    {
                        Vector3 _diferencaAltura = new Vector3(0, 3, 0);
                        Instantiate(GameManager._Instancia._FazendaNv2, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                        _slotClicado.name = 2.ToString();
                        _objetoAtual = GameObject.Find("Fazenda(Clone)");
                        _objetoAtual.name = "Fazenda" + _slotClicado.gameObject.name;
                        _objetoAtual.transform.SetParent(_slotClicado.transform);
                        _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                        igreja.comidaMax += 2;
                        igreja.CalculateLimits();
                        GameManager._Instancia._madeira -= 54;
                        GameManager._Instancia._barro -= 81;
                        GameManager._Instancia._pedra -= 60;
                        GameManager._Instancia._comidaSoma += 6;
                        SoundManager._Instancia.PlayEffect(_efeitoClick);
                        GameManager._Instancia.EscreverMensagem("Uma fazenda foi evoluída!");
                    }
                    else
                    {
                        SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                        GameManager._Instancia.EscreverMensagem("Você precisa de 54 unidades de madeira, 60 unidades de pedra e 81 unidades barro para contruir este edifício.");
                    }
                }
                else
                {
                    if (_slotClicado.name == "2")
                    {
                        if (GameManager._Instancia._madeira >= 145 && GameManager._Instancia._barro >= 109 && GameManager._Instancia._pedra >= 162)
                        {
                            Vector3 _diferencaAltura = new Vector3(0, 4, 0);
                            Instantiate(GameManager._Instancia._FazendaNv3, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                            _slotClicado.name = 3.ToString();
                            _objetoAtual = GameObject.Find("Fazenda(Clone)");
                            _objetoAtual.name = "Fazenda" + _slotClicado.gameObject.name;
                            _objetoAtual.transform.SetParent(_slotClicado.transform);
                            _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                            igreja.comidaMax += 3;
                            igreja.CalculateLimits();
                            GameManager._Instancia._madeira -= 145;
                            GameManager._Instancia._barro -= 109;
                            GameManager._Instancia._pedra -= 162;
                            GameManager._Instancia._comidaSoma += 14;
                            SoundManager._Instancia.PlayEffect(_efeitoClick);
                            GameManager._Instancia.EscreverMensagem("Uma fazenda foi evoluída!");
                        }
                        else
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Você precisa de 145 unidades de madeira, 162 unidades de pedra e 109 unidades barro para contruir este edifício.");
                        }
                    }
                    else
                    {
                        if (_slotClicado.name == "3")
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Este edifício já está no nível máximo.");
                        }
                    }
                }
            }
        }

        if (_construcao == "trade")
        {
            if (_slotClicado.name == "0")
            {
                if (GameManager._Instancia._madeira >= 60 && GameManager._Instancia._barro >= 55)
                {
                    Vector3 _diferencaAltura = new Vector3(0, 2, 0);
                    Instantiate(GameManager._Instancia._MercadoNv1, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                    _slotClicado.name = 1.ToString();
                    _objetoAtual = GameObject.Find("Mercado(Clone)");
                    _objetoAtual.name = "Mercado" + _slotClicado.gameObject.name;
                    _objetoAtual.transform.SetParent(_slotClicado.transform);
                    _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                    GameManager._Instancia._madeira -= 60;
                    GameManager._Instancia._barro -= 55;
                    SoundManager._Instancia.PlayEffect(_efeitoClick);
                    GameManager._Instancia.EscreverMensagem("O mercado foi construído!");
                }
                else
                {
                    SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                    GameManager._Instancia.EscreverMensagem("Você precisa de 60 unidades de madeira e 55 unidades barro para contruir este edifício.");
                }
            }
            else
            {
                if (_slotClicado.name == "1")
                {
                    if (GameManager._Instancia._madeira >= 159 && GameManager._Instancia._barro >= 145 && GameManager._Instancia._pedra >= 60)
                    {
                        Vector3 _diferencaAltura = new Vector3(0, 3, 0);
                        Instantiate(GameManager._Instancia._MercadoNv2, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                        _slotClicado.name = 2.ToString();
                        _objetoAtual = GameObject.Find("Mercado(Clone)");
                        _objetoAtual.name = "Mercado" + _slotClicado.gameObject.name;
                        _objetoAtual.transform.SetParent(_slotClicado.transform);
                        _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                        GameManager._Instancia._madeira -= 159;
                        GameManager._Instancia._barro -= 145;
                        GameManager._Instancia._pedra -= 60;
                        SoundManager._Instancia.PlayEffect(_efeitoClick);
                        GameManager._Instancia.EscreverMensagem("O mercado foi evoluído!");
                    }
                    else
                    {
                        SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                        GameManager._Instancia.EscreverMensagem("Você precisa de 159 unidades de madeira, 60 unidades de pedra e 145 unidades barro para contruir este edifício.");
                    }
                }
                else
                {
                    if (_slotClicado.name == "2")
                    {
                        if (GameManager._Instancia._madeira >= 421 && GameManager._Instancia._barro >= 193 && GameManager._Instancia._pedra >= 159)
                        {
                            Vector3 _diferencaAltura = new Vector3(0, 4, 0);
                            Instantiate(GameManager._Instancia._MercadoNv3, _slotClicado.transform.position + _diferencaAltura, Quaternion.Euler(-90, 45, 0));
                            _slotClicado.name = 3.ToString();
                            _objetoAtual = GameObject.Find("Mercado(Clone)");
                            _objetoAtual.name = "Mercado" + _slotClicado.gameObject.name;
                            _objetoAtual.transform.SetParent(_slotClicado.transform);
                            _slotClicado.GetComponent<OnClickSlot>()._objetoAtual = _objetoAtual;
                            GameManager._Instancia._madeira -= 421;
                            GameManager._Instancia._barro -= 193;
                            GameManager._Instancia._pedra -= 159;
                            SoundManager._Instancia.PlayEffect(_efeitoClick);
                            GameManager._Instancia.EscreverMensagem("O mercado foi evoluído!");
                        }
                        else
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Você precisa de 421 unidades de madeira, 159 unidades de pedra e 193 unidades barro para contruir este edifício.");
                        }
                    }
                    else
                    {
                        if (_slotClicado.name == "3")
                        {
                            SoundManager._Instancia.PlayEffect(_efeitoMensagem);
                            GameManager._Instancia.EscreverMensagem("Este edifício já está no nível máximo.");
                        }
                    }
                }
            }
        }
    }

    private void OnMouseDown()
    {
        
    }
}
