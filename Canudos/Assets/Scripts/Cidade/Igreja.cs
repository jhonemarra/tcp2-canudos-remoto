﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Igreja : MonoBehaviour {

    int populacaoTotal;
    int populacaoAtual;
    int populacaoDisponivel;
    int populacaoBarro;
    int populacaoPedra;
    int populacaoMadeira;
    int populacaoComida;
    public int barroMod, pedraMod, madeiraMod, comidaMod, barroMax, pedraMax, madeiraMax, comidaMax;

    public Image barroBar, pedraBar, madeiraBar, comidaBar;
    public Text barroBarText, pedraBarText, madeiraBarText, comidaBarText, populationText;

    public GameObject igrejaInterface;
    GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        CalculateLimits();
        CalculatePopulation();

    }

    private void Update()
    {

        CalculateBars();     

        if (igrejaInterface.active == true)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                igrejaInterface.SetActive(false);
            }
        }
        if (SceneManager.GetActiveScene().name != "Cidade")
        {
            igrejaInterface.SetActive(false);
        }



    }

    private void OnMouseDown()
    {
        igrejaInterface.SetActive(true);
    }

    void CalculateBars()
    {
       barroBar.fillAmount = gameManager._timer / 10.2f;
       pedraBar.fillAmount = gameManager._timer / 10.2f;
       madeiraBar.fillAmount = gameManager._timer / 10.2f;
       comidaBar.fillAmount = gameManager._timer / 10.2f;
    }

    public void CalculateLimits()
    {
        barroBarText.text = "Barro " + barroMod + "/" + barroMax;
        pedraBarText.text = "Pedra " + pedraMod + "/" + pedraMax;
        madeiraBarText.text = "Madeira " + madeiraMod + "/" + madeiraMax;
        comidaBarText.text = "Comida " + comidaMod + "/" + comidaMax;
    }
    public void CalculatePopulation()
    {
        populacaoDisponivel = gameManager._populacao - barroMod - pedraMod - madeiraMod - comidaMod;
        populationText.text = populacaoDisponivel + "/" + gameManager._populacao;
    }

    public void IncreaseBarro()
    {
        if (populacaoDisponivel > 0 && barroMax > 0 && barroMod < barroMax)
        {
            barroMod += 1;
        }

        barroBarText.text = "Barro " + barroMod + "/" + barroMax;
    }
    public void DecreaseBarro()
    {
        if (barroMod > 0 && barroMax > 0)
        {
            barroMod -= 1;
        }

        barroBarText.text = "Barro " + barroMod + "/" + barroMax;

    }
    public void IncreasePedra()
    {
        if (populacaoDisponivel > 0 && pedraMax > 0 && pedraMod < pedraMax)
        {
            pedraMod += 1;
        }
        pedraBarText.text = "Pedra " + pedraMod + "/" + pedraMax;

    }
    public void DecreasePedra()
    {
        if (pedraMod > 0 && pedraMax > 0)
        {
            pedraMod -= 1;
        }
        pedraBarText.text = "Pedra " + pedraMod + "/" + pedraMax;

    }
    public void IncreaseMadeira()
    {
        if (populacaoDisponivel > 0 && madeiraMax > 0 && madeiraMod < madeiraMax)
        {
            madeiraMod += 1;
        }
        madeiraBarText.text = "Madeira " + madeiraMod + "/" + madeiraMax;

    }
    public void DecreaseMadeira()
    {
        if (madeiraMod > 0 && madeiraMax > 0)
        {
            madeiraMod -= 1;
        }
        madeiraBarText.text = "Madeira " + madeiraMod + "/" + madeiraMax;

    }
    public void IncreaseComida()
    {
        if (populacaoDisponivel > 0 && comidaMax > 0 && comidaMod < comidaMax)
        {
            comidaMod += 1;
        }
        comidaBarText.text = "Comida " + comidaMod + "/" + comidaMax;

    }
    public void DecreaseComida()
    {
        if (comidaMod > 0 && comidaMax > 0)
        {
            comidaMod -= 1;
        }
        comidaBarText.text = "Comida " + comidaMod + "/" + comidaMax;

    }
}
