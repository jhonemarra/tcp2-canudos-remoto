﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour {
    public static SoundManager _Instancia;

    public AudioSource _audioSource;
    public AudioSource _OSTSource;
    public AudioClip battleP1;
    public AudioClip battleP2;

    // Start is called before the first frame update
    void Awake()
    {
        // evita o reset da estrutura
        if (_Instancia == null)
        {
            _Instancia = this;
        }
        else if (_Instancia != this)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if ( SceneManager.GetActiveScene().name == "Batalha")
        {
            if (_OSTSource.clip == null)
            {
                int sortedNumber = Random.Range(0, 2);
                if (sortedNumber == 0)
                {
                    _OSTSource.clip = battleP1;
                    _OSTSource.Play();
                }
                else
                {
                    _OSTSource.clip = battleP2;
                    _OSTSource.Play();
                }
            }

            if (_OSTSource.isPlaying == false)
            {
                _OSTSource.clip = null;
            }
        }
 
    }

    public void PlayEffect(AudioClip _effect, float _pitch)
    {
        _audioSource.clip = _effect;
        _audioSource.pitch = _pitch;
        _audioSource.Play();
    }
    public void PlayEffect(AudioClip _effect)
    {
        _audioSource.clip = _effect;
        _audioSource.pitch = 1;
        _audioSource.Play();
    }

}
