﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public bool onRange;
    public bool _MorreuGeral = false;
    public bool moveOn;
    public bool showAttribute;

    int movePreference = 1;
    public int moves;
    public int currentMoves;
    public int ataquesTotal;
    public int ataquesRestantes;

    public float disPlayer;
    public float disActivePlayer;
    public float HP;
    public float maxHP;
    public float active;
    float count;
    float forca, destreza, vitalidade, resistencia, energia;
    float _speed = 4;
    float disPlayerX;
    float disPlayerY;
    public Image hpBarImage;

    public AudioClip _attackClip;


    public Vector3 posNode1, posNode2, posNode3, posNode4, posNode5, posNode6, posNode7, posNode8, posNode9;
    Vector3 destiny;
    Camera _camera;
    Player[] players;
    public Player player;
    OnClickEnemy onclickenemy;
    OnClickEnemy[] onClicksEnemies;

    TurnManager turnManager;
    GameManager gameManager;


    enum STATE
    {
        RESETAR,
        AGUARDANDO,
        ATIVO,
    };

    STATE currentState = STATE.RESETAR;

    public string node1, node2, node3, node4, node5, node6, node7, node8, node9;
    public GameObject currentNode;
    public GUISkin textureBar;


    // Start is called before the first frame update
    void Start()
    {
        destiny = transform.position;
        //Os movimentos reais do enemy correspondem a moves - 1
        moves = 4;

        forca = 1;
        destreza = 1;
        vitalidade = 1;
        resistencia = 1;
        energia = 100;
        disPlayer = 100;

        turnManager = FindObjectOfType<TurnManager>();
        gameManager = FindObjectOfType<GameManager>();
        _camera = FindObjectOfType<Camera>();
        players = FindObjectsOfType<Player>();
        player = FindObjectOfType<Player>();
        onClicksEnemies = FindObjectsOfType<OnClickEnemy>();


        for (int i = 0; i < 8; i++)
        {
            int sortedAttribute = Random.Range(1, 5);
            if (sortedAttribute == 1)
            {
                forca += 1;
            }
            if (sortedAttribute == 2)
            {
                destreza += 1;
            }
            if (sortedAttribute == 3)
            {
                vitalidade += 1;
            }
            if (sortedAttribute == 4)
            {
                resistencia += 1;
            }
            if (sortedAttribute == 5)
            {
                energia += 1;
            }
            if (i == 7)
            {
                HP = 8 + Mathf.Floor(vitalidade * 2);
                maxHP = 8 + Mathf.Floor(vitalidade * 2);
                turnManager.enemiesMaxHP += maxHP;
                turnManager.enemiesHP += HP;
            }
        }


        for (int i = 0; i < gameManager.totalEnemies; i++)
        {
            if (Vector3.Distance(onClicksEnemies[i].transform.position, this.transform.position) < 3)
            {
                onclickenemy = onClicksEnemies[i];
            }
        }


        if (this.gameObject.name.Contains(1.ToString()))
        {
            turnManager.ReceiveEnemyIndex(1, this);
        }
        else if (this.gameObject.name.Contains(2.ToString()))
        {
            turnManager.ReceiveEnemyIndex(2, this);
        }
        else if (this.gameObject.name.Contains(3.ToString()))
        {
            turnManager.ReceiveEnemyIndex(3, this);
        }
        else if (this.gameObject.name.Contains(4.ToString()))
        {
            turnManager.ReceiveEnemyIndex(4, this);
        }
        else if (this.gameObject.name.Contains(5.ToString()))
        {
            turnManager.ReceiveEnemyIndex(5, this);
        }
        else if (this.gameObject.name.Contains(6.ToString()))
        {
            turnManager.ReceiveEnemyIndex(6, this);
        }
        else if (this.gameObject.name.Contains(7.ToString()))
        {
            turnManager.ReceiveEnemyIndex(7, this);
        }
        else if (this.gameObject.name.Contains(8.ToString()))
        {
            turnManager.ReceiveEnemyIndex(8, this);
        }
        else if(this.gameObject.name.Contains(9.ToString()))
        {
            turnManager.ReceiveEnemyIndex(9, this);
        }
        else
        {
            turnManager.ReceiveEnemyIndex(0, this);
        }

    }

    // Update is called once per frame
    void Update()
    {
        count -= 1 * Time.deltaTime;
        hpBarImage.fillAmount = HP / maxHP;

        if (FindObjectOfType<Node>().activePlayer != null)
        {
            disActivePlayer = Vector3.Distance(FindObjectOfType<Node>().activePlayer.transform.position, this.transform.position);

        }


        if (count < 0)
        {
            count = 0;
        }



        if (player == null)
        {
            for (int i = 0; i < gameManager.totalAllies; i++)
            {
                player = players[i];
            }
        }

        if (player != null)
        {
            for (int i = 0; i < gameManager.totalAllies; i++)
            {
                if (Vector3.Distance(this.transform.position, players[i].transform.position) < disPlayer)
                {
                    player = players[i];
                }
            }
        }

        try
        {
            for (int i = 0; i < gameManager.totalAllies; i++)
            {
                if (Vector3.Distance(this.transform.position, players[i].transform.position) < disPlayer)
                {
                    player = players[i];
                }
            }
        }
        catch
        {
            for (int i = 0; i < gameManager.totalAllies; i++)
            {
                player = null;
                disPlayer = 100;
            }
        }

        if (player != null)
        {
            disPlayerX = player.transform.position.x - transform.position.x;
            disPlayerY = player.transform.position.z - transform.position.z;
        }




        if (HP <= 0)
        {
            currentNode = null;
            onclickenemy.mouseOver = false;
            transform.position = new Vector3(0, 0, 0);
        }



        if (onclickenemy.mouseOver == true && Input.GetMouseButtonUp(0))
        {
            showAttribute = true;
        }
        if (showAttribute == true)
        {
            ShowAttribute();

            if (Input.GetKeyUp(KeyCode.Escape))
            {
                showAttribute = false;
                gameManager.backgroundAttribute.enabled = false;
                gameManager.vidaText.enabled = false;
                gameManager.forcaText.enabled = false;
                gameManager.destrezaText.enabled = false;
                gameManager.resistenciaText.enabled = false;
                gameManager.vitalidadeText.enabled = false;
                gameManager.energiaText.enabled = false;
            }
        }

        if (player != null)
        {
            disPlayer = Vector3.Distance(this.transform.position, player.transform.position);
        }

        if (disPlayerX > 0 && disPlayerY > 0)
        {
            if (disPlayerX > disPlayerY)
            {
                movePreference = 3;
            }
            else
            {
                movePreference = 2;
            }
        }
        if (disPlayerX < 0 && disPlayerY < 0)
        {
            if (disPlayerX < disPlayerY)
            {
                movePreference = 1;

            }
            else
            {
                movePreference = 4;

            }
        }
        if (disPlayerX > 0 && disPlayerY < 0)
        {
            if (disPlayerX > disPlayerY * -1)
            {
                movePreference = 3;
            }
            else
            {
                movePreference = 4;

            }
        }
        if (disPlayerX < 0 && disPlayerY > 0)
        {
            if (disPlayerX * -1 > disPlayerY)
            {
                movePreference = 2;

            }
            else
            {
                movePreference = 1;

            }
        }

        if (turnManager.enemyTurn == true && turnManager.playerTurn == false)
        {
            if (currentState == STATE.RESETAR)
            {
                moves = 4;
                currentState = STATE.AGUARDANDO;

                if (disPlayer > 3)
                {
                    ataquesRestantes = 3;
                }
                else
                {
                    ataquesRestantes = 4;
                }
            }

            if (moves > 0)
            {
                if (currentState == STATE.AGUARDANDO)
                {
                    currentMoves = moves;

                    int sortedNumber = Random.Range(1, 4);
                    if (movePreference == 1)
                    {
                        if (sortedNumber == 1)
                        {
                            destiny = posNode1;
                        }
                        if (sortedNumber == 2)
                        {

                            destiny = posNode4;
                        }
                        if (sortedNumber == 3)
                        {

                            destiny = posNode7;
                        }
                    }
                    else if (movePreference == 2)
                    {
                        if (sortedNumber == 1)
                        {
                            destiny = posNode1;
                        }
                        if (sortedNumber == 2)
                        {
                            destiny = posNode2;
                        }
                        if (sortedNumber == 3)
                        {
                            destiny = posNode3;
                        }
                    }
                    else if (movePreference == 3)
                    {
                        if (sortedNumber == 1)
                        {
                            destiny = posNode3;
                        }
                        if (sortedNumber == 2)
                        {
                            destiny = posNode6;
                        }
                        if (sortedNumber == 3)
                        {
                            destiny = posNode9;
                        }
                    }
                    else if (movePreference == 4)
                    {
                        if (sortedNumber == 1)
                        {
                            destiny = posNode7;
                        }
                        if (sortedNumber == 2)
                        {
                            destiny = posNode8;
                        }
                        if (sortedNumber == 3)
                        {
                            destiny = posNode9;
                        }
                    }
                    count = 1;
                    if (currentMoves - 1 > 0)
                    {
                        moves = currentMoves - 1;
                        ataquesRestantes -= 1;

                        currentState = STATE.ATIVO;
                    }
                    else
                    {
                        turnManager.ChangeTurn(0);
                    }
                }
                if (currentState == STATE.ATIVO)
                {
                    if (player != null)
                    {
                        if (disPlayer > 3 || Mathf.Abs(transform.position.x - posNode5.x) > 0.1 || Mathf.Abs(transform.position.z - posNode5.z) > 0.1)
                        {
                            if (moves > 0)
                            {
                                if (transform.position != destiny && player.transform.position != destiny)
                                {
                                    transform.position = Vector3.MoveTowards(transform.position, destiny, _speed * Time.deltaTime);

                                }
                                if (count <= 0)
                                {
                                    currentState = STATE.AGUARDANDO;
                                }
                            }
                            else
                            {
                                moves = 0;
                            }
                        }
                        else
                        {

                            if (ataquesRestantes > 0)
                            {
                                if (count <= 0)
                                {
                                    player.DamageReceive(forca);
                                    SoundManager._Instancia.PlayEffect(_attackClip, 0.2f);
                                    _camera.Shake(0.2f, 0.3f);
                                    currentState = STATE.AGUARDANDO;

                                }
                                if (count <= 0)
                                {
                                    currentState = STATE.AGUARDANDO;
                                }
                            }
                            else
                            {
                                moves = 0;
                            }
                        }
                    }

                }
            }
            else
            {
                turnManager.ChangeTurn(0);
            }
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject != player.gameObject && collider.gameObject.name.Contains("Enemy") == false)
        {
            if (collider.gameObject.transform.position.z > transform.position.z)
            {
                if (collider.gameObject.transform.position.x < transform.position.x)
                {
                    node1 = collider.name.ToString();
                }
                if (collider.gameObject.transform.position.x == transform.position.x)
                {
                    node2 = collider.name.ToString();

                }
                if (collider.gameObject.transform.position.x > transform.position.x)
                {
                    node3 = collider.name.ToString();

                }
            }


            if (collider.gameObject.transform.position.z == transform.position.z)
            {
                if (collider.gameObject.transform.position.x < transform.position.x)
                {
                    node4 = collider.name.ToString();

                }

                if (collider.gameObject.transform.position.x == transform.position.x)
                {
                    node5 = collider.name.ToString();
                    currentNode = collider.gameObject;
                }
                if (collider.gameObject.transform.position.x > transform.position.x)
                {
                    node6 = collider.name.ToString();

                }

            }
            if (collider.gameObject.transform.position.z < transform.position.z)
            {
                if (collider.gameObject.transform.position.x < transform.position.x)
                {
                    node7 = collider.name.ToString();

                }
                if (collider.gameObject.transform.position.x == transform.position.x)
                {
                    node8 = collider.name.ToString();

                }
                if (collider.gameObject.transform.position.x > transform.position.x)
                {
                    node9 = collider.name.ToString();

                }
            }
        }
    }
    void OnTriggerStay(Collider collider)
    {
        if (player != null)
        {
            if (collider.gameObject != player.gameObject && collider.gameObject.name.Contains("Enemy") == false)
            {
                if (collider.gameObject.transform.position.z > transform.position.z)
                {
                    if (collider.gameObject.transform.position.x < transform.position.x)
                    {
                        node1 = collider.name.ToString();
                    }
                    if (collider.gameObject.transform.position.x == transform.position.x)
                    {
                        node2 = collider.name.ToString();
                    }
                    if (collider.gameObject.transform.position.x > transform.position.x)
                    {
                        node3 = collider.name.ToString();
                    }
                }
                if (collider.gameObject.transform.position.z == transform.position.z)
                {
                    if (collider.gameObject.transform.position.x < transform.position.x)
                    {
                        node4 = collider.name.ToString();
                    }
                    if (collider.gameObject.transform.position.x == transform.position.x)
                    {
                        node5 = collider.name.ToString();
                        currentNode = collider.gameObject;

                    }
                    if (collider.gameObject.transform.position.x > transform.position.x)
                    {
                        node6 = collider.name.ToString();
                    }
                }
                if (collider.gameObject.transform.position.z < transform.position.z)
                {
                    if (collider.gameObject.transform.position.x < transform.position.x)
                    {
                        node7 = collider.name.ToString();
                    }
                    if (collider.gameObject.transform.position.x == transform.position.x)
                    {
                        node8 = collider.name.ToString();
                    }
                    if (collider.gameObject.transform.position.x > transform.position.x)
                    {
                        node9 = collider.name.ToString();
                    }
                }
            }
        }

    }
    void OnTriggerExit(Collider collider)
    {
        if (player != null)
        {
            if (collider.gameObject != player.gameObject)
            {
                clearNode();
            }
        }

    }

    private void OnGUI()
    {
        GUI.skin = textureBar;
    }

    public void clearNode()
    {
        node1 = null;
        node2 = null;
        node3 = null;
        node4 = null;
        node5 = null;
        node6 = null;
        node7 = null;
        node8 = null;
        node9 = null;
        currentNode = null;
    }

    public void DamageReceive(float damage)
    {
        if (HP > 0)
        {
            if (damage - (resistencia / 2) > 1)
            {
                float danoRecebido = (damage - ((int)resistencia / 2)) /gameManager.totalEnemies;
                this.HP = HP - danoRecebido;
                turnManager.enemiesHP = turnManager.enemiesHP - danoRecebido;

                if (HP - ((int)resistencia) / 2 <= -1)
                {
                    onclickenemy.mouseOver = false;
                }

            }
            else
            {
                this.HP = HP - 0.5f;
                turnManager.enemiesHP = turnManager.enemiesHP - 0.5f;

                if (HP - 1 <= 0)
                {
                    onclickenemy.mouseOver = false;
                    player.ativo = false;
                }
            }
        }
    }
    public void Reset()
    {
        currentState = STATE.RESETAR;
    }
    void ShowAttribute()
    {
        gameManager.backgroundAttribute.enabled = true;
        gameManager.vidaText.enabled = true;
        gameManager.vidaText.text = "Vida : " + HP.ToString() + " / " + maxHP.ToString();
        gameManager.forcaText.enabled = true;
        gameManager.forcaText.text = "Força : " + forca.ToString();
        gameManager.destrezaText.enabled = true;
        gameManager.destrezaText.text = "Destreza : " + destreza.ToString();
        gameManager.resistenciaText.enabled = true;
        gameManager.resistenciaText.text = "Resistência : " + resistencia.ToString();
        gameManager.vitalidadeText.enabled = true;
        gameManager.vitalidadeText.text = "Vitalidade : " + vitalidade.ToString();
        gameManager.energiaText.enabled = true;
        gameManager.energiaText.text = "Energia : " + energia.ToString();

    }

}
