﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    Color DefaultColor;
    Player[] players;
    Player player;
    Enemy[] enemies;
    Enemy enemy;
    TurnManager turnManager;
    GameManager gameManager;
    public GameObject activePlayer;
    public GameObject currentCharacter;

    public float disPlayer;


    public bool adjacente;
    bool mouseOver;

    // Start is called before the first frame update
    void Start()
    {
        DefaultColor = gameObject.GetComponent<MeshRenderer>().material.color;
        players = FindObjectsOfType<Player>();
        player = FindObjectOfType<Player>();
        enemy = FindObjectOfType<Enemy>();
        enemies = FindObjectsOfType<Enemy>();
        turnManager = FindObjectOfType<TurnManager>();
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

        if (player == null)
        {
            player = FindObjectOfType<Player>();
        }

        if (player != null)
        {
            for (int i = 0; i < gameManager.totalAllies; i++)
            {
                if (players[i].ativo == true)
                {
                    player = players[i];
                }
            }
        }

        if (enemy == null)
        {
            enemy = FindObjectOfType<Enemy>();
        }

        try 
        {
            if (player.gameObject.name == activePlayer.name)
            {
                disPlayer = Vector3.Distance(transform.position, player.transform.position);
            }
        }
        catch
        {
            activePlayer = null;
        }


        if (mouseOver == true)
        {
            gameObject.GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255);
        }

        else if (mouseOver == false && adjacente == false)
        {
            gameObject.GetComponent<MeshRenderer>().material.color = DefaultColor;
        }

        if (activePlayer != null && disPlayer <= player.moves * 2 && disPlayer != 0 && turnManager.playerTurn == true)
        {
            adjacente = true;
        }
        else
        {
            adjacente = false;
        }

        for (int i = 0; i < gameManager.totalEnemies; i++)
        {
            enemy = enemies[i];

            if (mouseOver == false)
            {
                if (activePlayer != null)
                {
                    if (currentCharacter != null && Vector3.Distance(activePlayer.transform.position, this.transform.position) < 3)
                    {
                        gameObject.GetComponent<MeshRenderer>().material.color = new Color(255, 0, 0);
                    }
                    else if (adjacente == true && enemies[i].currentNode != this.gameObject)
                    {
                        gameObject.GetComponent<MeshRenderer>().material.color = new Color(0, 255, 0);
                    }
                }
            }

            if (this.adjacente == true && mouseOver == true)
            {
                if (activePlayer != null)
                {
                    if (Input.GetMouseButtonUp(1) && player.moves > 0 && currentCharacter == null)
                    {
                        activePlayer.GetComponentInChildren<Animator>().SetInteger("walking", 1);

                        if (disPlayer <= 2)
                        {
                            player._destino = new Vector3(transform.position.x, player.transform.position.y, transform.position.z);
                            player.moves -= 0.5f;
                        }
                        if (disPlayer > 2 && disPlayer <= 5)
                        {
                            player._destino = new Vector3(transform.position.x, player.transform.position.y, transform.position.z);
                            player.moves -= 1;
                        }
                        if (disPlayer > 5 && disPlayer <= 6)
                        {
                            player._destino = new Vector3(transform.position.x, player.transform.position.y, transform.position.z);
                            player.moves -= 1.5f;
                        }

                    }
                }
            }

            if (this.gameObject.name.ToString() == enemy.node1)
            {
                enemy.posNode1 = this.transform.position;
            }
            if (this.gameObject.name.ToString() == enemy.node2)
            {
                enemy.posNode2 = this.transform.position;
            }
            if (this.gameObject.name.ToString() == enemy.node3)
            {
                enemy.posNode3 = this.transform.position;
            }
            if (this.gameObject.name.ToString() == enemy.node4)
            {
                enemy.posNode4 = this.transform.position;
            }
            if (this.gameObject.name.ToString() == enemy.node5)
            {
                enemy.posNode5 = this.transform.position;
            }
            if (this.gameObject.name.ToString() == enemy.node6)
            {
                enemy.posNode6 = this.transform.position;
            }
            if (this.gameObject.name.ToString() == enemy.node7)
            {
                enemy.posNode7 = this.transform.position;
            }
            if (this.gameObject.name.ToString() == enemy.node8)
            {
                enemy.posNode8 = this.transform.position;
            }
            if (this.gameObject.name.ToString() == enemy.node9)
            {
                enemy.posNode9 = this.transform.position;
            }
        }

        if (currentCharacter != null)
        {
            if (Vector3.Distance(this.transform.position, currentCharacter.transform.position) > 1.3)
            {
                currentCharacter = null;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            activePlayer.gameObject.GetComponentInChildren<Animator>().SetInteger("walking",1);
        }

    }
    private void OnTriggerEnter(Collider collider)
    {
        if (Vector3.Distance(this.transform.position, collider.transform.position) < 2)
        {
            currentCharacter = collider.gameObject;
        }

    }
    private void OnTriggerStay(Collider collider)
    {
        if (Vector3.Distance(this.transform.position, collider.transform.position) < 2)
        {
            currentCharacter = collider.gameObject;
        }
    }

    private void OnMouseOver()
    {
        mouseOver = true;
    }
    private void OnMouseExit()
    {
        mouseOver = false;
    }
}
