﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    Player[] players;
    Enemy[] enemies;
    Node[] nodes;
    TurnManager turnManager;
    GameManager gameManager;
    public GameObject ancinho, enxada, foice, espingarda, facao, martelo, revolver;
    public AudioClip _endBattleClip, _messageClip;
    public bool endLevel = false;
    bool showReward = true;

    // Start is called before the first frame update
    void Start()
    {
        enemies = FindObjectsOfType<Enemy>();
        players = FindObjectsOfType<Player>();
        nodes = FindObjectsOfType<Node>();
        turnManager = FindObjectOfType<TurnManager>();
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (turnManager.alliesHP <= 0 || turnManager.enemiesHP <= 0)
        {
            endLevel = true;
        }
    }

    private void OnGUI()
    {
        if (GUI.Button(new Rect(Screen.width - Screen.width / 5, Screen.height - Screen.height / 5, Screen.width / 5, Screen.height / 10), "Finalizar Turno") && turnManager.playerTurn == true && turnManager.enemyTurn == false)
        {
            turnManager.playerTurn = false;
            turnManager.enemyTurn = true;

            for (int i = 0; i < gameManager.totalAllies; i++)
            {
                players[i].halo.enabled = false;
                players[i].ativo = false;
            }

            for (int i = 0; i < 77; i++)
            {
                nodes[i].activePlayer = null;
            }

            turnManager.ChangeTurn(1);
        }

        if (GUI.Button(new Rect(Screen.width - Screen.width / 5, Screen.height - Screen.height / 10f, Screen.width / 5, Screen.height / 10), "Finalizar Batalha") && endLevel == true)
        {
            SoundManager._Instancia.PlayEffect(_endBattleClip);
            SceneManager.LoadScene("Cidade");
            GameManager._Instancia._habilitarRecursos = true;
            GameManager._Instancia._habilitarConstrucoes = true;
            GameManager._Instancia.ContagemRounds();
            endLevel = false;
        }
        if (GUI.Button(new Rect(Screen.width - Screen.width / 5, Screen.height - Screen.height / 10f, Screen.width / 5, Screen.height / 10), "Finalizar Batalha") && endLevel == false)
        {
            ////Coloquei pra finalizar a batalha mesmo sem derrotar os inimigos 
            ////porque o bug de entrar uma peça dentro da outra tava tornando difícil
            ////derrotar todos eles. Mas ainda assim não funfou o botão.
            //SoundManager._Instancia.PlayEffect(_endBattleClip);
            //SceneManager.LoadScene("Cidade");
            //GameManager._Instancia._habilitarRecursos = true;
            //GameManager._Instancia._habilitarConstrucoes = true;
            //GameManager._Instancia.ContagemRounds();
            //endLevel = false;

            SoundManager._Instancia.PlayEffect(_messageClip);
            GameManager._Instancia.EscreverMensagem("A batalha ainda não acabou !");
            Debug.Log("inimigos");

        }

        if (turnManager.enemiesHP <= 0)
        {
            GUI.DrawTexture(new Rect(Screen.width / 2 - (Screen.width / 1.5f / 2), Screen.height / 7 - (Screen.height / 1.5f / 2), Screen.width / 1.5f, Screen.height / 1.5f), Resources.Load<Texture2D>("vitoriaimagempng") as Texture2D, ScaleMode.ScaleToFit, true);
            GUI.DrawTexture(new Rect(Screen.width / 2 - (Screen.width / 1.5f / 2), Screen.height / 2.8f -(Screen.height / 1.5f / 2), Screen.width / 1.5f, Screen.height / 1.5f), Resources.Load<Texture2D>("itensobtidospng") as Texture2D, ScaleMode.ScaleToFit, true);

            GUI.Box(new Rect(Screen.width - Screen.width/10 * 9, Screen.height - Screen.height / 10 * 6, Screen.width /10 * 8, Screen.height / 10 * 6), " ");

            if (showReward == true)
            {

                for (int i = 0; i < 4; i++)
                {
                    if (i < 3)
                    {
                        Reward(i);
                    }
                }
                showReward = false;
            }
        }
        else if (turnManager.alliesHP <= 0)
        {
            GUI.DrawTexture(new Rect(Screen.width / 2 - (Screen.width / 1.5f / 2), Screen.height / 6 - (Screen.height / 1.5f / 2), Screen.width / 1.5f, Screen.height / 1.5f), Resources.Load<Texture2D>("derrotaimagempng") as Texture2D, ScaleMode.ScaleToFit, true);
        }
    }
    public void Reward(int i)
    {

        float sortedNumber = Random.Range(0, 100);
        if (sortedNumber > 40)
        {

            float sortedReward = Random.Range(0, 100);
            {
                if (sortedReward <= 15)
                {
                    Instantiate(ancinho, new Vector3(-2 + (i * 2), 29, -32), Quaternion.Euler(-48, 0, 0));
                }
                else if (sortedReward > 15 && sortedReward <= 30)
                {
                    Instantiate(enxada, new Vector3(-2 + (i * 2), 30, -32), Quaternion.Euler(-48, 0, 0));

                }
                else if (sortedReward > 30 && sortedReward <= 45)
                {
                    Instantiate(foice, new Vector3(-3 + (i * 2), 28, -32), Quaternion.Euler(-48, 0, 0));

                }
                else if (sortedReward > 45 && sortedReward <= 60)
                {
                    Instantiate(martelo, new Vector3(-2 + (i * 2), 31, -32), Quaternion.Euler(-48, 0, 0));

                }
                else if (sortedReward > 60 && sortedReward <= 70)
                {
                    Instantiate(facao, new Vector3(-2 + (i * 2), 30, -32), Quaternion.Euler(-48, 0, 0));

                }
                else if (sortedReward > 70 && sortedReward <= 80)
                {
                    
                    Instantiate(revolver, new Vector3(-2 + (i * 2), 31, -32), Quaternion.Euler(-48, 0, 0));

                }
                else if (sortedReward > 80 && sortedReward <= 90)
                {
                    Instantiate(espingarda, new Vector3(-2 + (i * 2), 30, -32), Quaternion.Euler(-48, 0, 0));

                }
                else if (sortedReward > 90 && sortedReward <= 100)
                {
                    //Rifle
                    //Instantiate(facao, new Vector3(-3 + (i * 2), 28, -31), Quaternion.Euler(-48, 0, 0));

                }
            }
        }

        sortedNumber = -1;
    }
}
