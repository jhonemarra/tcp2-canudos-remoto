﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickPlayer : MonoBehaviour {

    public bool mouseOver;

    private void OnMouseOver()
    {
        mouseOver = true;
    }
    private void OnMouseExit()
    {
        mouseOver = false;
    }

}
