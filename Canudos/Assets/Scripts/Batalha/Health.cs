﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour

{
    Enemy enemy;
    float max;
    float min;

    // Start is called before the first frame update
    void Start()
    {
        enemy = FindObjectOfType<Enemy>();
        
    }

    // Update is called once per frame
    void Update()
    {
        max = enemy.maxHP;
        min = enemy.HP;
        transform.localScale = new Vector3(20 * (min / max), 2, 10);
    }
}
