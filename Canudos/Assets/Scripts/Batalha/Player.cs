﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float moves, _speed;
    public float HP;
    public float maxHP;
    float forca, destreza, vitalidade, resistencia, energia;
    //public string node1, node2, node3, node4, node5, node6, node7, node8, node9;

    Vector3 posNode1, posNode2, posNode3, posNode4, posNode5, posNode6, posNode7, posNode8, posNode9;
    Enemy[] enemies;
    OnClickEnemy onClickEnemy;
    OnClickEnemy[] onClicksEnemies;
    OnClickPlayer[] onClicksPlayers;
    OnClickPlayer onClickPlayer;
    Camera _camera;
    Node[] node;
    TurnManager turnManager;
    GameManager gameManager;
    public Behaviour halo;
    public AudioClip _selectClip, _unselectClip, _resetClip, _attackClip;
    public Vector3 _destino;
    public GameObject currentNode;
    public Image hpBarImage;

    public bool ativo = true;


    public enum STATE
    {
        RESETAR,
        AGUARDANDO,
        ATIVO,
    };
    public STATE currentState = STATE.AGUARDANDO;

    // Start is called before the first frame update
    void Start()
    {
        moves = 3;

        forca = 1;
        destreza = 1;
        vitalidade = 1;
        resistencia = 1;
        energia = 100;

        _destino = transform.position;
        halo.enabled = false;
        ativo = false;

        enemies = FindObjectsOfType<Enemy>();
        onClicksEnemies = FindObjectsOfType<OnClickEnemy>();
        onClicksPlayers = FindObjectsOfType<OnClickPlayer>();
        _camera = FindObjectOfType<Camera>();
        node = FindObjectsOfType<Node>();
        turnManager = FindObjectOfType<TurnManager>();
        gameManager = FindObjectOfType<GameManager>();


        for (int i = 0; i < 10; i++)
        {
            int sortedAttribute = Random.Range(1, 5);
            if (sortedAttribute == 1)
            {
                forca += 1;
            }
            if (sortedAttribute == 2)
            {
                destreza += 1;
            }
            if (sortedAttribute == 3)
            {
                vitalidade += 1;
            }
            if (sortedAttribute == 4)
            {
                resistencia += 1;
            }
            if (sortedAttribute == 5)
            {
                energia += 1;
            }
            if (i == 9)
            {
                HP = 8 + Mathf.Floor(vitalidade * 2);
                maxHP = 8 + Mathf.Floor(vitalidade * 2);
                turnManager.alliesMaxHP += maxHP;
                turnManager.alliesHP += HP;
            }
        }




        for (int i = 0; i < gameManager.totalAllies; i++)
        {
            if (Vector3.Distance(onClicksPlayers[i].transform.position, this.transform.position) < 3)
            {
                onClickPlayer = onClicksPlayers[i];
            }
        }

        currentState = STATE.AGUARDANDO;
    }

    // Update is called once per frame
    void Update()
    {
        hpBarImage.fillAmount = HP / maxHP;

        if (HP <= 0)
        {
            currentNode = null;
            transform.position = new Vector3(0, 0, 0);
        }

        if (GetComponent<Player>().name == this.name)
        {

            if (currentState == STATE.RESETAR && turnManager.playerTurn == true)
            {
                moves = 3;
                //Behaviour halo = (Behaviour)gameObject.GetComponent("Halo");
                currentState = STATE.AGUARDANDO;
                ativo = false;
                //SoundManager._Instancia.PlayEffect(_resetClip);
            }

            if (currentState == STATE.AGUARDANDO)
            {
                //Behaviour halo = (Behaviour)gameObject.GetComponent("Halo");

                if (onClickPlayer.mouseOver == true && Input.GetMouseButtonUp(0))
                {
                    for (int i = 0; i < 77; i++)
                    {
                        node[i].activePlayer = this.gameObject;
                    }
                    SoundManager._Instancia.PlayEffect(_selectClip);
                    currentState = STATE.ATIVO;
    
                }
            }


            if (currentState == STATE.ATIVO && turnManager.playerTurn == true)
            {
                //Behaviour halo = (Behaviour)gameObject.GetComponent("Halo");
                ativo = true;
                halo.enabled = true;
                
                for (int i = 0; i < gameManager.totalEnemies; i++)
                {
                    for (int j = 0; j < gameManager.totalEnemies; j++)
                    {
                        if (this.ativo == true && enemies[i].disActivePlayer <= 2.83 && Input.GetMouseButtonUp(1) && onClicksEnemies[i].mouseOver == true && Vector3.Distance(this.transform.position, onClicksEnemies[i].transform.position) < 5 && moves > 0)
                        {
                            moves -= 0.5f;
                            enemies[i].DamageReceive(forca);
                            SoundManager._Instancia.PlayEffect(_attackClip);
                            _camera.Shake(0.2f, 0.3f);

                            this.gameObject.GetComponentInChildren<Animator>().SetInteger("attacking", 1);
                        }
                        else
                        {
                            this.gameObject.GetComponentInChildren<Animator>().SetInteger("attacking", 0);
                        }
                    }

                }

                if (Input.GetKeyUp(KeyCode.I))
                {
                    moves += 3;
                }

                for (int i = 0; i < gameManager.totalAllies; i++)
                {
                    if (Input.GetKeyUp(KeyCode.Escape) || Input.GetMouseButtonUp(0) && onClickPlayer.mouseOver == false)
                    {
                        for (int j = 0; j < 77; j++)
                        {
                            node[j].activePlayer = null;
                        }
                        this.halo.enabled = false;
                        this.ativo = false;
                        this.currentState = STATE.AGUARDANDO;
                        SoundManager._Instancia.PlayEffect(_unselectClip);
                        gameManager.backgroundAttribute.enabled = false;
                        gameManager.vidaText.enabled = false;
                        gameManager.forcaText.enabled = false;
                        gameManager.destrezaText.enabled = false;
                        gameManager.resistenciaText.enabled = false;
                        gameManager.vitalidadeText.enabled = false;
                        gameManager.energiaText.enabled = false;
                    }
                }
            }

            if (currentState == STATE.ATIVO)
            {
                ShowAttribute();
            }

            if (transform.position != _destino)
            {
                transform.position = Vector3.MoveTowards(transform.position, _destino, _speed);
            }
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject)
        {
            currentNode = collider.gameObject;
        }
    }
    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject)
        {
            currentNode = collider.gameObject;
        }
    }

    public void Reset()
    {
        currentState = STATE.RESETAR;
    }
    public void DamageReceive(float damage)
    {
        if (HP > 0)
        {
            if (damage - ((int)resistencia) / 2 >= 1)
            {
                this.HP = HP - (damage - ((int)(this.resistencia / 2)));
                turnManager.alliesHP -= damage - ((int)(this.resistencia / 2));

                if (HP - ((int)resistencia) / 2 <= -1)
                {
                    onClickPlayer.mouseOver = false;
                }
            }
            else
            {
                this.HP = HP - 1;
                turnManager.alliesHP -= 1;


                if (HP - 1 <= -1)
                {
                    onClickPlayer.mouseOver = false;
                }
            }
        }
    }
    void ShowAttribute()
    {
        gameManager.backgroundAttribute.enabled = true;
        gameManager.vidaText.enabled = true;
        gameManager.vidaText.text = "Vida : " + HP.ToString() + " / " + maxHP.ToString();
        gameManager.forcaText.enabled = true;
        gameManager.forcaText.text = "Força : " + forca.ToString();
        gameManager.destrezaText.enabled = true;
        gameManager.destrezaText.text = "Destreza : " + destreza.ToString();
        gameManager.resistenciaText.enabled = true;
        gameManager.resistenciaText.text = "Resistência : " + resistencia.ToString();
        gameManager.vitalidadeText.enabled = true;
        gameManager.vitalidadeText.text = "Vitalidade : " + vitalidade.ToString();
        gameManager.energiaText.enabled = true;
        gameManager.energiaText.text = "Energia : " + energia.ToString();
    }
}
