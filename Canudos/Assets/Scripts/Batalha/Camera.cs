﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    public float intensidade;
    public float duracao;
    public Transform _camera;

    Vector3 posInicial;
    float tempoInicial;


    // Start is called before the first frame update
    void Start()
    {
        _camera = transform;
        posInicial = _camera.localPosition;
        tempoInicial = duracao;
    }

    // Update is called once per frame
    void Update()
    {
        if (duracao > 0)
        {
            _camera.localPosition = posInicial + Random.insideUnitSphere * intensidade;
            duracao -= Time.deltaTime;
        }
        else
        {
            duracao = tempoInicial;
            _camera.localPosition = posInicial;
        }
    }

    public void Shake(float intensidade, float duracao)
    {
        this.duracao = duracao;
        this.intensidade = intensidade;
    }
}
