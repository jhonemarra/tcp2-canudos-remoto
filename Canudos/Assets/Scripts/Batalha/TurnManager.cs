﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{

    public bool playerTurn;
    public bool enemyTurn;
    public float alliesMaxHP = 0;
    public float enemiesMaxHP = 0;
    public float alliesHP = 0;
    public float enemiesHP = 0;
    Player[] players;
    Enemy[] enemies;
    GameManager gameManager;


    // Start is called before the first frame update
    void Start()
    {
        players = FindObjectsOfType<Player>();
        gameManager = FindObjectOfType<GameManager>();
        enemies = new Enemy[gameManager.totalEnemies];

        playerTurn = true;
        enemyTurn = false;

    }

    private void Update()
    {
        Debug.Log("allies  :  " + alliesHP);
        Debug.Log("enemies : " + enemiesHP);
    }

    // Update is called once per frame

    public void ChangeTurn(int Who)
    {
        if (Who == 0)
        {
            enemyTurn = false;
            playerTurn = true;

            for (int i = 0; i < gameManager.totalAllies; i++)
            {
                players[i].Reset();
            }
        }
        if (Who == 1)
        {
            enemyTurn = true;
            playerTurn = false;

            for (int i = 0; i < gameManager.totalEnemies; i++)
            {
                if (enemies[i] != null)
                {
                    enemies[i].Reset();
                }
            }

        }
    }
    public void ReceiveEnemyIndex(int index, Enemy enemy)
    {
        this.enemies[index] = enemy;
    }
}
