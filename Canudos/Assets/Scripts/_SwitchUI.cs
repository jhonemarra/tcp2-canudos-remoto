﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class _SwitchUI : MonoBehaviour {

    public Sprite sprite1;
    public Sprite sprite2;
    public bool mouseOver;

	// Use this for initialization
	void Start ()
    {
        sprite1 = this.GetComponent<Image>().sprite;
    }

    public void Sprite1()
    {
        GetComponent<Image>().sprite = sprite1;
        GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 175);

    }
    public void Sprite2()
    {
        GetComponent<Image>().sprite = sprite2;
        GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 275);

    }
}
